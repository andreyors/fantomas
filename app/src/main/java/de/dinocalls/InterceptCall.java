package de.dinocalls;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class InterceptCall extends BroadcastReceiver {
    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Date callStartTime;
    private static boolean isIncoming;
    private final OkHttpClient client = new OkHttpClient();

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String number;

            if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
                number = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");

                Toast.makeText(context, number, Toast.LENGTH_LONG).show();
            } else {
                number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);

                String stateText = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);

                int state;
                if (stateText.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_IDLE)) {
                    state = TelephonyManager.CALL_STATE_IDLE;
                } else if (stateText.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    state = TelephonyManager.CALL_STATE_OFFHOOK;
                } else if (stateText.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {
                    state = TelephonyManager.CALL_STATE_RINGING;
                } else {
                    state = 0;
                }

                onCallStateChanged(context, state, number);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCallStateChanged(Context context, int state, String number) {
        String cname = makePost(number);

        if (lastState == state) {
            return;
        }

        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;

                onIncomingCallStarted(context, number, new Date(), cname);
                break;

            case TelephonyManager.CALL_STATE_OFFHOOK:
                if (isIncoming) {
                    onIncomingCallEnded(context, number, callStartTime, new Date());
                }
                break;

            case TelephonyManager.CALL_STATE_IDLE:
                if (isIncoming) {
                    onIncomingCallEnded(context, number, callStartTime, new Date());
                }
                break;
        }

        lastState = state;
    }

    protected void onIncomingCallStarted(Context ctx, String number, Date start, String name) {
        final Context context;
        context = ctx;

        final Intent intent = new Intent(context, ContactDialog.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_SINGLE_TOP |
                Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("contact_name", name);
        intent.putExtra("phone_no", number);

        callStartTime = start;

        context.startActivity(intent);
    }

    protected void onIncomingCallEnded(Context context, String number, Date stop, Date end) {
        long diffInMillies = Math.abs(stop.getTime() - callStartTime.getTime());
        long diff = TimeUnit.SECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        // Toast.makeText(context, diff + " seconds", Toast.LENGTH_LONG).show();

        ContactDialog.mActivity.finish();
    }

    protected String makePost(String number)  {
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("number", number)
                .build();

        Request request = new Request.Builder()
                .url("http://3.85.165.28:8080/number_exists")
                .post(requestBody)
                .build();

        try  {
            Response response = client.newCall(request).execute();

            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            Headers responseHeaders = response.headers();
            for (int i = 0; i < responseHeaders.size(); i++) {
                System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
            }

            String result = response.body().string().trim();

            JSONObject jObj = new JSONObject(result);

            return jObj.getString("company");
        } catch (Exception e) {
            return "";
        }
    }
}
