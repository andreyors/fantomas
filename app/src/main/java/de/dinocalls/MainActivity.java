package de.dinocalls;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.READ_CALL_LOG,
                        Manifest.permission.PROCESS_OUTGOING_CALLS,
                        Manifest.permission.SYSTEM_ALERT_WINDOW,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.WAKE_LOCK,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE
                ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {}
                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {}
                }).check();


        StrictMode.ThreadPolicy policy = new
        StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); //You can manage the blinking time with this parameter
        anim.setStartOffset(200);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);

        Button button = findViewById(R.id.blink);
        button.startAnimation(anim);
    }

    public void onClickTickets(View v) {
        Intent intent = new Intent(getApplicationContext(), Tickets.class);

        startActivity(intent);
    }

    public void onClickEmail(View v) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"contract@openbank.de"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Re: New contract");
        intent.putExtra(Intent.EXTRA_TEXT, "Hi Michael, we discussed the new contract. You can pay us till 28.03.2019\n\nMfG, Andrey");

        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    public void onCallNow(View v) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:+491751261257"));

        startActivity(intent);
    }

    public void onClickCompany(View v) {
        Intent intent = new Intent(getApplicationContext(), Emails.class);

        startActivity(intent);
    }

    public void onClickCrossSell(View v) {
        Intent intent = new Intent(getApplicationContext(), CrossSell.class);

        startActivity(intent);
    }
}
