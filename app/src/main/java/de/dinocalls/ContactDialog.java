package de.dinocalls;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

public class ContactDialog extends AppCompatActivity {
    public static Activity mActivity;

    TextView Cname, Cnumber;
    String contactName, contactNumber, id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.setFinishOnTouchOutside(true);

            setContentView(R.layout.activity_contact_dialog);

            this.mActivity = this;

            initializeContent();

            contactNumber = getIntent().getExtras().getString("phone_no");
            contactName = getIntent().getExtras().getString("contact_name");

            if (contactName == null) {
                Cname.setText("Unknown number");
                Cnumber.setText(contactNumber);
            } else {
                Cname.setText("" + contactName + " is calling you");
                Cnumber.setText(contactNumber);
            }

            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500); //You can manage the blinking time with this parameter
            anim.setStartOffset(200);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);

            Button button = findViewById(R.id.blink);
            button.startAnimation(anim);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeContent() {
        Cname = (TextView) findViewById(R.id.Cname);
        Cnumber = (TextView) findViewById(R.id.Cnumber);
    }
}

